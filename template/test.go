package template

import (
	"gitlab.com/karamelsoft/goconfig/v2"
	"gitlab.com/karamelsoft/goconfig/v2/inmemory"
)

func NewTestConfiguration(properties map[string]any) goconfig.Configuration {
	return inmemory.NewConfiguration(properties)
}
