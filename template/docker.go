package template

import (
	"gitlab.com/karamelsoft/goconfig/v2"
	"gitlab.com/karamelsoft/goconfig/v2/docker"
	"gitlab.com/karamelsoft/goconfig/v2/environment"
	"gitlab.com/karamelsoft/goconfig/v2/fallback"
)

func NewDockerConfiguration() goconfig.Configuration {
	return fallback.NewConfiguration(
		docker.NewConfiguration(),
		environment.NewConfiguration(),
	)
}

func NewDockerConfigurationUsing(fileName string) func() goconfig.Configuration {
	return func() goconfig.Configuration {
		return fallback.NewConfiguration(
			docker.NewConfigurationUsing(fileName)(),
			environment.NewConfiguration(),
		)
	}
}
