package template

import (
	"gitlab.com/karamelsoft/goconfig/v2"
	"gitlab.com/karamelsoft/goconfig/v2/bound"
	"gitlab.com/karamelsoft/goconfig/v2/cli"
	"gitlab.com/karamelsoft/goconfig/v2/environment"
	"gitlab.com/karamelsoft/goconfig/v2/fallback"
)

func NewCliConfiguration(applicationName string, bindings ...bound.Binding) goconfig.Configuration {
	return fallback.NewConfiguration(
		bound.NewConfiguration(bindings...),
		fallback.NewConfiguration(
			cli.NewConfiguration(applicationName),
			environment.NewConfiguration(),
		),
	)
}
