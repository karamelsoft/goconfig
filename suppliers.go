package goconfig

var (
	missing = func(name string) {
		Crash("missing mandatory property:", name)
	}
)

func SetMissingFunction(behaviour func(name string)) {
	missing = behaviour
}

func OrMissing[T any](name string) func() T {
	return func() T {
		var value T
		missing(name)
		return value
	}
}

func Or[T any](value T) func() T {
	return func() T {
		return value
	}
}
