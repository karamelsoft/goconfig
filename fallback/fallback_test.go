package fallback

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/karamelsoft/goconfig/v2/docker"
	"testing"
)

func _TestFallbackConfiguration(t *testing.T) {
	conf := docker.NewConfiguration()
	value := conf.GetString("UNKNOWN", func() string {
		return "default"
	})

	assert.Equal(t, "default", value)
}
