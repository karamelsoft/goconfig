package fallback

import "gitlab.com/karamelsoft/goconfig/v2"

func NewConfiguration(primary, secondary goconfig.Configuration) *Configuration {
	goconfig.Log("creating fallback configuration")

	return &Configuration{
		primary:   primary,
		secondary: secondary,
	}
}

type Configuration struct {
	primary   goconfig.Configuration
	secondary goconfig.Configuration
}

func (c *Configuration) GetString(name string, defaultSupplier func() string) string {
	return c.primary.GetString(name, func() string {
		return c.secondary.GetString(name, defaultSupplier)
	})
}

func (c *Configuration) GetStringOrCrash(name string) string {
	return c.primary.GetString(name, func() string {
		return c.secondary.GetStringOrCrash(name)
	})
}

func (c *Configuration) GetStringArray(name string, defaultSupplier func() []string) []string {
	return c.primary.GetStringArray(name, func() []string {
		return c.secondary.GetStringArray(name, defaultSupplier)
	})
}

func (c *Configuration) GetStringArrayOrCrash(name string) []string {
	return c.primary.GetStringArray(name, func() []string {
		return c.secondary.GetStringArrayOrCrash(name)
	})
}

func (c *Configuration) GetBool(name string, defaultSupplier func() bool) bool {
	return c.primary.GetBool(name, func() bool {
		return c.secondary.GetBool(name, defaultSupplier)
	})
}

func (c *Configuration) GetBoolOrCrash(name string) bool {
	return c.primary.GetBool(name, func() bool {
		return c.secondary.GetBoolOrCrash(name)
	})
}

func (c *Configuration) GetBoolArray(name string, defaultSupplier func() []bool) []bool {
	return c.primary.GetBoolArray(name, func() []bool {
		return c.secondary.GetBoolArray(name, defaultSupplier)
	})
}

func (c *Configuration) GetBoolArrayOrCrash(name string) []bool {
	return c.primary.GetBoolArray(name, func() []bool {
		return c.secondary.GetBoolArrayOrCrash(name)
	})
}

func (c *Configuration) GetInt(name string, defaultSupplier func() int) int {
	return c.primary.GetInt(name, func() int {
		return c.secondary.GetInt(name, defaultSupplier)
	})
}

func (c *Configuration) GetIntOrCrash(name string) int {
	return c.primary.GetInt(name, func() int {
		return c.secondary.GetIntOrCrash(name)
	})
}

func (c *Configuration) GetIntArray(name string, defaultSupplier func() []int) []int {
	return c.primary.GetIntArray(name, func() []int {
		return c.secondary.GetIntArray(name, defaultSupplier)
	})
}

func (c *Configuration) GetIntArrayOrCrash(name string) []int {
	return c.primary.GetIntArray(name, func() []int {
		return c.secondary.GetIntArrayOrCrash(name)
	})
}

func (c *Configuration) GetInt32(name string, defaultSupplier func() int32) int32 {
	return c.primary.GetInt32(name, func() int32 {
		return c.secondary.GetInt32(name, defaultSupplier)
	})
}

func (c *Configuration) GetInt32OrCrash(name string) int32 {
	return c.primary.GetInt32(name, func() int32 {
		return c.secondary.GetInt32OrCrash(name)
	})
}

func (c *Configuration) GetInt32Array(name string, defaultSupplier func() []int32) []int32 {
	return c.primary.GetInt32Array(name, func() []int32 {
		return c.secondary.GetInt32Array(name, defaultSupplier)
	})
}

func (c *Configuration) GetInt32ArrayOrCrash(name string) []int32 {
	return c.primary.GetInt32Array(name, func() []int32 {
		return c.secondary.GetInt32ArrayOrCrash(name)
	})
}

func (c *Configuration) GetInt64(name string, defaultSupplier func() int64) int64 {
	return c.primary.GetInt64(name, func() int64 {
		return c.secondary.GetInt64(name, defaultSupplier)
	})
}

func (c *Configuration) GetInt64OrCrash(name string) int64 {
	return c.primary.GetInt64(name, func() int64 {
		return c.secondary.GetInt64OrCrash(name)
	})
}

func (c *Configuration) GetInt64Array(name string, defaultSupplier func() []int64) []int64 {
	return c.primary.GetInt64Array(name, func() []int64 {
		return c.secondary.GetInt64Array(name, defaultSupplier)
	})
}

func (c *Configuration) GetInt64ArrayOrCrash(name string) []int64 {
	return c.primary.GetInt64Array(name, func() []int64 {
		return c.secondary.GetInt64ArrayOrCrash(name)
	})
}

func (c *Configuration) GetFloat32(name string, defaultSupplier func() float32) float32 {
	return c.primary.GetFloat32(name, func() float32 {
		return c.secondary.GetFloat32(name, defaultSupplier)
	})
}

func (c *Configuration) GetFloat32OrCrash(name string) float32 {
	return c.primary.GetFloat32(name, func() float32 {
		return c.secondary.GetFloat32OrCrash(name)
	})
}

func (c *Configuration) GetFloat32Array(name string, defaultSupplier func() []float32) []float32 {
	return c.primary.GetFloat32Array(name, func() []float32 {
		return c.secondary.GetFloat32Array(name, defaultSupplier)
	})
}

func (c *Configuration) GetFloat32ArrayOrCrash(name string) []float32 {
	return c.primary.GetFloat32Array(name, func() []float32 {
		return c.secondary.GetFloat32ArrayOrCrash(name)
	})
}

func (c *Configuration) GetFloat64(name string, defaultSupplier func() float64) float64 {
	return c.primary.GetFloat64(name, func() float64 {
		return c.secondary.GetFloat64(name, defaultSupplier)
	})
}

func (c *Configuration) GetFloat64OrCrash(name string) float64 {
	return c.primary.GetFloat64(name, func() float64 {
		return c.secondary.GetFloat64OrCrash(name)
	})
}

func (c *Configuration) GetFloat64Array(name string, defaultSupplier func() []float64) []float64 {
	return c.primary.GetFloat64Array(name, func() []float64 {
		return c.secondary.GetFloat64Array(name, defaultSupplier)
	})
}

func (c *Configuration) GetFloat64ArrayOrCrash(name string) []float64 {
	return c.primary.GetFloat64Array(name, func() []float64 {
		return c.secondary.GetFloat64ArrayOrCrash(name)
	})
}

func (c *Configuration) GetProperties(name string, defaultSupplier func() map[string]string) map[string]string {
	return c.primary.GetProperties(name, func() map[string]string {
		return c.secondary.GetProperties(name, defaultSupplier)
	})
}

func (c *Configuration) GetPropertiesOrCrash(name string) map[string]string {
	return c.primary.GetProperties(name, func() map[string]string {
		return c.secondary.GetPropertiesOrCrash(name)
	})
}
