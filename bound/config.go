package bound

import "gitlab.com/karamelsoft/goconfig/v2"

func NewConfiguration(bindings ...Binding) *Configuration {
	goconfig.Log("creating binding configuration")

	bindins := make(map[string]func() any)
	for _, binding := range bindings {
		bindins[binding.Key] = binding.Value
	}

	return &Configuration{
		bindings: bindins,
	}
}

func Bind(key string, value func() any) Binding {
	return Binding{
		Key:   key,
		Value: value,
	}
}

type Binding struct {
	Key   string
	Value func() any
}

type Configuration struct {
	bindings map[string]func() any
}

func get[T any](c *Configuration, name string) *property[T] {
	return newProperty[T](c.bindings[name])
}

func (c *Configuration) Set(name string, provider func() any) {
	c.bindings[name] = provider
}

func (c *Configuration) Unset(name string) {
	delete(c.bindings, name)
}

func (c *Configuration) GetString(name string, defaultSupplier func() string) string {
	return get[string](c, name).or(defaultSupplier)
}

func (c *Configuration) GetStringOrCrash(name string) string {
	return get[string](c, name).or(goconfig.OrMissing[string](name))
}

func (c *Configuration) GetStringArray(name string, defaultSupplier func() []string) []string {
	return get[[]string](c, name).or(defaultSupplier)
}

func (c *Configuration) GetStringArrayOrCrash(name string) []string {
	return get[[]string](c, name).or(goconfig.OrMissing[[]string](name))
}

func (c *Configuration) GetBool(name string, defaultSupplier func() bool) bool {
	return get[bool](c, name).or(defaultSupplier)
}

func (c *Configuration) GetBoolOrCrash(name string) bool {
	return get[bool](c, name).or(goconfig.OrMissing[bool](name))
}

func (c *Configuration) GetBoolArray(name string, defaultSupplier func() []bool) []bool {
	return get[[]bool](c, name).or(defaultSupplier)
}

func (c *Configuration) GetBoolArrayOrCrash(name string) []bool {
	return get[[]bool](c, name).or(goconfig.OrMissing[[]bool](name))
}

func (c *Configuration) GetInt(name string, defaultSupplier func() int) int {
	return get[int](c, name).or(defaultSupplier)
}

func (c *Configuration) GetIntOrCrash(name string) int {
	return get[int](c, name).or(goconfig.OrMissing[int](name))
}

func (c *Configuration) GetIntArray(name string, defaultSupplier func() []int) []int {
	return get[[]int](c, name).or(defaultSupplier)
}

func (c *Configuration) GetIntArrayOrCrash(name string) []int {
	return get[[]int](c, name).or(goconfig.OrMissing[[]int](name))
}

func (c *Configuration) GetInt32(name string, defaultSupplier func() int32) int32 {
	return get[int32](c, name).or(defaultSupplier)
}

func (c *Configuration) GetInt32OrCrash(name string) int32 {
	return get[int32](c, name).or(goconfig.OrMissing[int32](name))
}

func (c *Configuration) GetInt32Array(name string, defaultSupplier func() []int32) []int32 {
	return get[[]int32](c, name).or(defaultSupplier)
}

func (c *Configuration) GetInt32ArrayOrCrash(name string) []int32 {
	return get[[]int32](c, name).or(goconfig.OrMissing[[]int32](name))
}

func (c *Configuration) GetInt64(name string, defaultSupplier func() int64) int64 {
	return get[int64](c, name).or(defaultSupplier)
}

func (c *Configuration) GetInt64OrCrash(name string) int64 {
	return get[int64](c, name).or(goconfig.OrMissing[int64](name))
}

func (c *Configuration) GetInt64Array(name string, defaultSupplier func() []int64) []int64 {
	return get[[]int64](c, name).or(defaultSupplier)
}

func (c *Configuration) GetInt64ArrayOrCrash(name string) []int64 {
	return get[[]int64](c, name).or(goconfig.OrMissing[[]int64](name))
}

func (c *Configuration) GetFloat32(name string, defaultSupplier func() float32) float32 {
	return get[float32](c, name).or(defaultSupplier)
}

func (c *Configuration) GetFloat32OrCrash(name string) float32 {
	return get[float32](c, name).or(goconfig.OrMissing[float32](name))
}

func (c *Configuration) GetFloat32Array(name string, defaultSupplier func() []float32) []float32 {
	return get[[]float32](c, name).or(defaultSupplier)
}

func (c *Configuration) GetFloat32ArrayOrCrash(name string) []float32 {
	return get[[]float32](c, name).or(goconfig.OrMissing[[]float32](name))
}

func (c *Configuration) GetFloat64(name string, defaultSupplier func() float64) float64 {
	return get[float64](c, name).or(defaultSupplier)
}

func (c *Configuration) GetFloat64OrCrash(name string) float64 {
	return get[float64](c, name).or(goconfig.OrMissing[float64](name))
}

func (c *Configuration) GetFloat64Array(name string, defaultSupplier func() []float64) []float64 {
	return get[[]float64](c, name).or(defaultSupplier)
}

func (c *Configuration) GetFloat64ArrayOrCrash(name string) []float64 {
	return get[[]float64](c, name).or(goconfig.OrMissing[[]float64](name))
}

func (c *Configuration) GetProperties(name string, defaultSupplier func() map[string]string) map[string]string {
	return get[map[string]string](c, name).or(defaultSupplier)
}

func (c *Configuration) GetPropertiesOrCrash(name string) map[string]string {
	return get[map[string]string](c, name).or(goconfig.OrMissing[map[string]string](name))
}
