package bound

func Ptr[T any](ptr *T) func() any {
	return func() any {
		return *ptr
	}
}
