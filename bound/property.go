package bound

import "reflect"

func newProperty[T any](provider func() any) *property[T] {
	return &property[T]{
		provider: provider,
	}
}

type property[T any] struct {
	provider func() any
}

func (p *property[T]) or(provider func() T) T {
	if p.provider == nil {
		return provider()
	}

	var empty T
	value := p.provider()
	if value == nil || reflect.DeepEqual(value, empty) {
		return provider()
	}

	return (value).(T)
}
