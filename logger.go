package goconfig

import "log"

var (
	logging  = log.Println
	crashing = log.Fatalln
)

func DeactivateLogging() {
	logging = func(values ...any) {
	}
}

func SetLoggingFunction(logger func(values ...any)) {
	logging = logger
}

func SetCrashingFunction(crash func(values ...any)) {
	crashing = crash
}

func Log(values ...any) {
	logging(values...)
}

func Crash(values ...any) {
	crashing(values...)
}
