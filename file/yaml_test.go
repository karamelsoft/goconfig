package file

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestFileConfiguration(t *testing.T) {
	assert := assert.New(t)
	config := NewConfiguration("test_config.yml")

	assert.Equal(2, config.Size())
}

func TestFileConfigurationIfExists(t *testing.T) {
	assert := assert.New(t)
	config := NewConfigurationIfExists("unknown.yml")

	assert.Equal(0, config.Size())
}
