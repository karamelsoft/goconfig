package file

import (
	"gitlab.com/karamelsoft/goconfig/v2"
	"gitlab.com/karamelsoft/goconfig/v2/inmemory"
	"gopkg.in/yaml.v3"
	"log"
	"os"
)

const (
	Mode                     = "file"
	configurationFilePath    = "CONFIGURATION_FILE_PATH"
	DefaultConfigurationFile = "configuration.yaml"
)

func NewConfigurationFrom(configuration goconfig.Configuration) *inmemory.Configuration {
	return NewConfiguration(configuration.GetStringOrCrash(configurationFilePath))
}

func NewConfiguration(path string) *inmemory.Configuration {
	goconfig.Log("creating file configuration with file path:", path)

	content, err := os.ReadFile(path)
	if err != nil {
		log.Fatalf("could not read file %s: %v", path, err)
	}

	properties := make(map[string]any)
	if err := yaml.Unmarshal(content, &properties); err != nil {
		log.Fatalln("could not read yaml content: ", err)
	}

	return inmemory.NewConfiguration(properties)
}

func NewConfigurationIfExists(path string) *inmemory.Configuration {
	goconfig.Log("creating file configuration with file path if exists:", path)

	properties := make(map[string]any)
	content, err := os.ReadFile(path)
	if err != nil {
		return inmemory.NewConfiguration(properties)
	}

	if err := yaml.Unmarshal(content, &properties); err != nil {
		log.Fatalln("could not read yaml content: ", err)
	}

	return inmemory.NewConfiguration(properties)
}
