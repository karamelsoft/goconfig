VERSION=v2.2.0

version:
	@echo $(VERSION)

clean:
	-go clean -testcache

build:
	go build -v ./...

test:
	go test -v -cover ./... || go test -v -cover ./...

tag:
	git tag -a $(VERSION) -m "Release $(VERSION)"

tag-force:
	git tag -a $(VERSION) -m "Release $(VERSION)" --force

push-tag:
	git push origin $(VERSION)

push-tag-force:
	git push origin $(VERSION) --force

release: tag push-tag

release-force: tag-force push-tag-force
