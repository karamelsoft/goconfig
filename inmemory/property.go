package inmemory

func newProperty[T any](value any, present bool) property[T] {
	return property[T]{
		value:   value,
		present: present,
	}
}

type property[T any] struct {
	value   any
	present bool
}

func (p property[T]) or(provider func() T) T {
	if !p.present || p.value == nil {
		return provider()
	}

	return p.value.(T)
}
