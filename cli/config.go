package cli

import (
	"gitlab.com/karamelsoft/goconfig/v2"
	"gitlab.com/karamelsoft/goconfig/v2/file"
	"gitlab.com/karamelsoft/goconfig/v2/inmemory"
	"log"
	"os"
	"path"
)

const (
	Mode = "cli"
)

func NewConfiguration(applicationName string) *inmemory.Configuration {
	goconfig.Log("creating cli configuration for application name:", applicationName)

	return file.NewConfigurationIfExists(homeConfigurationFile(applicationName))
}

func homeConfigurationFile(applicationName string) string {
	homeFolder, err := os.UserHomeDir()
	if err != nil {
		log.Fatalln("could not get home folder: ", err)
	}

	return path.Join(homeFolder, "."+applicationName, "config")
}
