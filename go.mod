module gitlab.com/karamelsoft/goconfig/v2

go 1.21

require (
	github.com/joho/godotenv v1.5.1
	github.com/stretchr/testify v1.7.0
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
)
