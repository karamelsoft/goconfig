# goconfig

How to manage configurations in Golang

## Download

```shell
go get gitlab.com/karamelsoft/goconfig/v2
```

## Usage

### Concept

The idea of `goconfig` is to manage configurations in a simple
and modular way using `key` and `type` for all the 
implementation that `goconfig` provides.

```go
var config goconfig.Configuration
//...
//gives "local.db" if the configuration variable is missing
conf.GetString("SQLITE_FILE", goconfig.Or("local.db"))
//crashes if the configuration variable is missing
conf.GetStringOrCrash("SQLITE_FILE") 
```

### Definition

```go
type Configuration interface {
    
    GetString(name string, defaultSupplier func() string) string
    GetStringOrCrash(name string) string
    GetStringArray(name string, defaultSupplier func() []string) []string
    GetStringArrayOrCrash(name string) []string
    
    GetBool(name string, defaultSupplier func() bool) bool
    GetBoolOrCrash(name string) bool
    GetBoolArray(name string, defaultSupplier func() []bool) []bool
    GetBoolArrayOrCrash(name string) []bool
    
    GetInt(name string, defaultSupplier func() int) int
    GetIntOrCrash(name string) int
    GetIntArray(name string, defaultSupplier func() []int) []int
    GetIntArrayOrCrash(name string) []int
    
    GetInt32(name string, defaultSupplier func() int32) int32
    GetInt32OrCrash(name string) int32
    GetInt32Array(name string, defaultSupplier func() []int32) []int32
    GetInt32ArrayOrCrash(name string) []int32
    
    GetInt64(name string, defaultSupplier func() int64) int64
    GetInt64OrCrash(name string) int64
    GetInt64Array(name string, defaultSupplier func() []int64) []int64
    GetInt64ArrayOrCrash(name string) []int64
    
    GetFloat32(name string, defaultSupplier func() float32) float32
    GetFloat32OrCrash(name string) float32
    GetFloat32Array(name string, defaultSupplier func() []float32) []float32
    GetFloat32ArrayOrCrash(name string) []float32
    
    GetFloat64(name string, defaultSupplier func() float64) float64
    GetFloat64OrCrash(name string) float64
    GetFloat64Array(name string, defaultSupplier func() []float64) []float64
    GetFloat64ArrayOrCrash(name string) []float64
    
    GetProperties(name string, defaultSupplier func() map[string]string) map[string]string
    GetPropertiesOrCrash(name string) map[string]string
}
```

### Implementations

#### Bound

Bound configuration provides a way to use provider and bind keys
to those providers. Useful if you want to bind CLI framework flags
for instance.

#### CLI

The configuration reads the yaml file `~/.<application_name>/config`
and provides its content as configuration.

```go
config := cli.NewConfiguration("myApplication")
```

#### Docker

Docker secrets are stored in `/run/secrets` folder as a file named as 
the secret named. By defaults `goconfig` is using `configuration.yaml`
but you can also use your own file name.

```go
config := docker.NewConfiguration()
config := docker.NewConfigurationUsing("my_secret")() //returns a high order function
```

#### Environment

```go
config := environment.NewConfiguration()
```

#### InMemory (Testing purpose)

```go
config := inmemory.NewConfiguration()
```

#### File (YAML)

```yaml
URL: "www.google.com"
PORT: "80"
```

```go
config := file.NewConfiguration(filePath)
```

#### Fallback

A fallback configuration has a primary and a secondary configuration.

```go
config := fallback.NewConfiguration(primary, secondary)
```

#### Factory 

Will use the environment variable ```CONFIGURATION_MODE``` to detect the
configuration mode to use

```go
config := factory.NewConfiguration("myApplication")
```

#### Templates

The `template` package provides convenient predefined configuration
for different environment:

###### CLI

```go
config := template.NewCliConfiguration("generator",
    bound.Bind(database.UrlProperty, bound.Ptr(&database.Url)),
),
```

###### Docker

```go
config := template.NewDockerConfiguration(),
config := template.NewDockerConfigurationUsing("my_secrets")(),
```

###### Test

```go
config := template.NewTestConfigurationUsing(),
```
