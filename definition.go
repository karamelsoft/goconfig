package goconfig

type Configuration interface {
	GetString(name string, defaultSupplier func() string) string
	GetStringOrCrash(name string) string
	GetStringArray(name string, defaultSupplier func() []string) []string
	GetStringArrayOrCrash(name string) []string

	GetBool(name string, defaultSupplier func() bool) bool
	GetBoolOrCrash(name string) bool
	GetBoolArray(name string, defaultSupplier func() []bool) []bool
	GetBoolArrayOrCrash(name string) []bool

	GetInt(name string, defaultSupplier func() int) int
	GetIntOrCrash(name string) int
	GetIntArray(name string, defaultSupplier func() []int) []int
	GetIntArrayOrCrash(name string) []int

	GetInt32(name string, defaultSupplier func() int32) int32
	GetInt32OrCrash(name string) int32
	GetInt32Array(name string, defaultSupplier func() []int32) []int32
	GetInt32ArrayOrCrash(name string) []int32

	GetInt64(name string, defaultSupplier func() int64) int64
	GetInt64OrCrash(name string) int64
	GetInt64Array(name string, defaultSupplier func() []int64) []int64
	GetInt64ArrayOrCrash(name string) []int64

	GetFloat32(name string, defaultSupplier func() float32) float32
	GetFloat32OrCrash(name string) float32
	GetFloat32Array(name string, defaultSupplier func() []float32) []float32
	GetFloat32ArrayOrCrash(name string) []float32

	GetFloat64(name string, defaultSupplier func() float64) float64
	GetFloat64OrCrash(name string) float64
	GetFloat64Array(name string, defaultSupplier func() []float64) []float64
	GetFloat64ArrayOrCrash(name string) []float64

	GetProperties(name string, defaultSupplier func() map[string]string) map[string]string
	GetPropertiesOrCrash(name string) map[string]string
}
