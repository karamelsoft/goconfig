package environment

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/karamelsoft/goconfig/v2"
	"testing"
)

func TestEnvironmentConfiguration(t *testing.T) {
	assert := assert.New(t)
	key := "name"
	tests := []struct {
		name      string
		value     string
		operation func(configuration Configuration) any
		want      any
	}{
		{
			name:  "GetString() when property exists",
			value: "value",
			operation: func(configuration Configuration) any {
				return configuration.GetString(key, goconfig.Or("DefaultValue"))
			},
			want: "value",
		},
		{
			name:  "GetString() when property does not exists",
			value: "",
			operation: func(configuration Configuration) any {
				return configuration.GetString(key, goconfig.Or("DefaultValue"))
			},
			want: "DefaultValue",
		},
		{
			name:  "GetStringOrCrash() when property exists",
			value: "value",
			operation: func(configuration Configuration) any {
				return configuration.GetStringOrCrash(key)
			},
			want: "value",
		},
		{
			name:  "GetStringArray() when property exists",
			value: "value1,value2",
			operation: func(configuration Configuration) any {
				return configuration.GetStringArray(key, goconfig.Or([]string{"DefaultValue1", "DefaultValue2"}))
			},
			want: []string{"value1", "value2"},
		},
		{
			name:  "GetStringArray() when property does not exists",
			value: "",
			operation: func(configuration Configuration) any {
				return configuration.GetStringArray(key, goconfig.Or([]string{"DefaultValue1", "DefaultValue2"}))
			},
			want: []string{"DefaultValue1", "DefaultValue2"},
		},
		{
			name:  "GetBool() when property exists",
			value: "true",
			operation: func(configuration Configuration) any {
				return configuration.GetBool(key, goconfig.Or(false))
			},
			want: true,
		},
		{
			name:  "GetBool() when property does not exists",
			value: "",
			operation: func(configuration Configuration) any {
				return configuration.GetBool(key, goconfig.Or(true))
			},
			want: true,
		},
		{
			name:  "GetBoolOrCrash() when property exists",
			value: "true",
			operation: func(configuration Configuration) any {
				return configuration.GetBoolOrCrash(key)
			},
			want: true,
		},
		{
			name:  "GetBoolArray() when property exists",
			value: "true,false,true",
			operation: func(configuration Configuration) any {
				return configuration.GetBoolArray(key, goconfig.Or([]bool{false, true, false}))
			},
			want: []bool{true, false, true},
		},
		{
			name:  "GetBoolArray() when property does not exists",
			value: "",
			operation: func(configuration Configuration) any {
				return configuration.GetBoolArray(key, goconfig.Or([]bool{false, true, false}))
			},
			want: []bool{false, true, false},
		},
		{
			name:  "GetBoolArrayOrCrash() when property exists",
			value: "true,false,true",
			operation: func(configuration Configuration) any {
				return configuration.GetBoolArrayOrCrash(key)
			},
			want: []bool{true, false, true},
		},
		{
			name:  "GetInt() when property exists",
			value: "12",
			operation: func(configuration Configuration) any {
				return configuration.GetInt(key, goconfig.Or(42))
			},
			want: 12,
		},
		{
			name:  "GetInt() when property does not exists",
			value: "",
			operation: func(configuration Configuration) any {
				return configuration.GetInt(key, goconfig.Or(42))
			},
			want: 42,
		},
		{
			name:  "GetIntOrCrash() when property exists",
			value: "12",
			operation: func(configuration Configuration) any {
				return configuration.GetIntOrCrash(key)
			},
			want: 12,
		},
		{
			name:  "GetIntArray() when property exists",
			value: "1,2",
			operation: func(configuration Configuration) any {
				return configuration.GetIntArray(key, goconfig.Or([]int{2, 3}))
			},
			want: []int{1, 2},
		},
		{
			name:  "GetIntArray() when property does not exists",
			value: "",
			operation: func(configuration Configuration) any {
				return configuration.GetIntArray(key, goconfig.Or([]int{2, 3}))
			},
			want: []int{2, 3},
		},
		{
			name:  "GetIntArrayOrCrash() when property exists",
			value: "1,2",
			operation: func(configuration Configuration) any {
				return configuration.GetIntArrayOrCrash(key)
			},
			want: []int{1, 2},
		},
		{
			name:  "GetInt32() when property exists",
			value: "12",
			operation: func(configuration Configuration) any {
				return configuration.GetInt32(key, goconfig.Or(int32(42)))
			},
			want: int32(12),
		},
		{
			name:  "GetInt32() when property does not exists",
			value: "",
			operation: func(configuration Configuration) any {
				return configuration.GetInt32(key, goconfig.Or(int32(42)))
			},
			want: int32(42),
		},
		{
			name:  "GetInt32OrCrash() when property exists",
			value: "12",
			operation: func(configuration Configuration) any {
				return configuration.GetInt32OrCrash(key)
			},
			want: int32(12),
		},
		{
			name:  "GetInt32Array() when property exists",
			value: "1,2",
			operation: func(configuration Configuration) any {
				return configuration.GetInt32Array(key, goconfig.Or([]int32{2, 3}))
			},
			want: []int32{1, 2},
		},
		{
			name:  "GetInt32Array() when property does not exists",
			value: "",
			operation: func(configuration Configuration) any {
				return configuration.GetInt32Array(key, goconfig.Or([]int32{2, 3}))
			},
			want: []int32{2, 3},
		},
		{
			name:  "GetInt32ArrayOrCrash() when property exists",
			value: "1,2",
			operation: func(configuration Configuration) any {
				return configuration.GetInt32ArrayOrCrash(key)
			},
			want: []int32{1, 2},
		},
		{
			name:  "GetInt64() when property exists",
			value: "12",
			operation: func(configuration Configuration) any {
				return configuration.GetInt64(key, goconfig.Or(int64(42)))
			},
			want: int64(12),
		},
		{
			name:  "GetInt64() when property does not exists",
			value: "",
			operation: func(configuration Configuration) any {
				return configuration.GetInt64(key, goconfig.Or(int64(42)))
			},
			want: int64(42),
		},
		{
			name:  "GetInt64OrCrash() when property exists",
			value: "12",
			operation: func(configuration Configuration) any {
				return configuration.GetInt64OrCrash(key)
			},
			want: int64(12),
		},
		{
			name:  "GetInt64Array() when property exists",
			value: "1,2",
			operation: func(configuration Configuration) any {
				return configuration.GetInt64Array(key, goconfig.Or([]int64{2, 3}))
			},
			want: []int64{1, 2},
		},
		{
			name:  "GetInt64Array() when property does not exists",
			value: "",
			operation: func(configuration Configuration) any {
				return configuration.GetInt64Array(key, goconfig.Or([]int64{2, 3}))
			},
			want: []int64{2, 3},
		},
		{
			name:  "GetInt64ArrayOrCrash() when property exists",
			value: "1,2",
			operation: func(configuration Configuration) any {
				return configuration.GetInt64ArrayOrCrash(key)
			},
			want: []int64{1, 2},
		},
		{
			name:  "GetFloat32() when property exists",
			value: "12.05",
			operation: func(configuration Configuration) any {
				return configuration.GetFloat32(key, goconfig.Or(float32(42.12)))
			},
			want: float32(12.05),
		},
		{
			name:  "GetFloat32() when property does not exists",
			value: "",
			operation: func(configuration Configuration) any {
				return configuration.GetFloat32(key, goconfig.Or(float32(42.12)))
			},
			want: float32(42.12),
		},
		{
			name:  "GetFloat32OrCrash() when property exists",
			value: "12.05",
			operation: func(configuration Configuration) any {
				return configuration.GetFloat32OrCrash(key)
			},
			want: float32(12.05),
		},
		{
			name:  "GetFloat32Array() when property exists",
			value: "12.05,50.21",
			operation: func(configuration Configuration) any {
				return configuration.GetFloat32Array(key, goconfig.Or([]float32{42.12, 42.12}))
			},
			want: []float32{12.05, 50.21},
		},
		{
			name:  "GetFloat32Array() when property does not exists",
			value: "",
			operation: func(configuration Configuration) any {
				return configuration.GetFloat32Array(key, goconfig.Or([]float32{42.12, 42.12}))
			},
			want: []float32{42.12, 42.12},
		},
		{
			name:  "GetFloat32ArrayOrCrash() when property exists",
			value: "12.05,50.21",
			operation: func(configuration Configuration) any {
				return configuration.GetFloat32ArrayOrCrash(key)
			},
			want: []float32{12.05, 50.21},
		},
		{
			name:  "GetFloat64() when property exists",
			value: "12.05",
			operation: func(configuration Configuration) any {
				return configuration.GetFloat64(key, goconfig.Or(42.12))
			},
			want: float64(12.05),
		},
		{
			name:  "GetFloat64() when property does not exists",
			value: "",
			operation: func(configuration Configuration) any {
				return configuration.GetFloat64(key, goconfig.Or(42.12))
			},
			want: float64(42.12),
		},
		{
			name:  "GetFloat64OrCrash() when property exists",
			value: "12.05",
			operation: func(configuration Configuration) any {
				return configuration.GetFloat64OrCrash(key)
			},
			want: float64(12.05),
		},
		{
			name:  "GetFloat64Array() when property exists",
			value: "12.05,50.21",
			operation: func(configuration Configuration) any {
				return configuration.GetFloat64Array(key, goconfig.Or([]float64{42.12, 42.12}))
			},
			want: []float64{12.05, 50.21},
		},
		{
			name:  "GetFloat64Array() when property does not exists",
			value: "",
			operation: func(configuration Configuration) any {
				return configuration.GetFloat64Array(key, goconfig.Or([]float64{42.12, 42.12}))
			},
			want: []float64{42.12, 42.12},
		},
		{
			name:  "GetFloat64ArrayOrCrash() when property exists",
			value: "12.05,50.21",
			operation: func(configuration Configuration) any {
				return configuration.GetFloat64ArrayOrCrash(key)
			},
			want: []float64{12.05, 50.21},
		},
		{
			name:  "GetProperties() when property exists",
			value: "{\"one\": \"ONE\",\"two\": \"TWO\"}",
			operation: func(configuration Configuration) any {
				return configuration.GetProperties(key, goconfig.Or(map[string]string{
					"one": "ONE",
					"two": "TWO",
				}))
			},
			want: map[string]string{
				"one": "ONE",
				"two": "TWO",
			},
		},
		{
			name:  "GetProperties() when property does not exists",
			value: "",
			operation: func(configuration Configuration) any {
				return configuration.GetProperties(key, goconfig.Or(map[string]string{
					"one": "ONE",
					"two": "TWO",
				}))
			},
			want: map[string]string{
				"one": "ONE",
				"two": "TWO",
			},
		},
		{
			name:  "GetPropertiesOrCrash() when property exists",
			value: "{\"one\": \"ONE\",\"two\": \"TWO\"}",
			operation: func(configuration Configuration) any {
				return configuration.GetPropertiesOrCrash(key)
			},
			want: map[string]string{
				"one": "ONE",
				"two": "TWO",
			},
		},
	}

	config := NewConfiguration()
	for _, tt := range tests {
		if err := config.Set(key, tt.value); err != nil {
			t.Fatal("could not set property: ", err)
		}
		assert.Equal(tt.want, tt.operation(config), fmt.Sprintf("%s failed", tt.name))
		_ = config.Set(key, "")
	}
}
