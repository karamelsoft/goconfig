package environment

import (
	"log"
	"strconv"
)

type boolProperty struct {
	defined bool
	value   bool
}

func (p boolProperty) or(supplier func() bool) bool {
	if p.defined {
		return p.value
	}

	return supplier()
}

type boolArrayProperty struct {
	value []bool
}

func (p boolArrayProperty) or(supplier func() []bool) []bool {
	if p.value == nil {
		return supplier()
	}

	return p.value
}

func toBool(name, value string) bool {
	result, err := strconv.ParseBool(value)
	if err != nil {
		log.Fatalf("could not convert property (%s = %s) to bool: %v", name, value, err)
	}

	return result
}

func toBoolArray(name, value string) []bool {
	array := toStringArray(value)
	result := make([]bool, 0, len(array))
	for _, item := range array {
		result = append(result, toBool(name, item))
	}

	return result
}
