package environment

import (
	"log"
	"strconv"
)

type intProperty struct {
	defined bool
	value   int
}

func (p intProperty) or(supplier func() int) int {
	if p.defined {
		return p.value
	}

	return supplier()
}

type intArrayProperty struct {
	value []int
}

func (p intArrayProperty) or(supplier func() []int) []int {
	if p.value == nil {
		return supplier()
	}

	return p.value
}

func toInt(name, value string) int {
	result, err := strconv.Atoi(value)
	if err != nil {
		log.Fatalf("could not convert property (%s = %s ) to int: %v", name, value, err)
	}

	return result
}

func toIntArray(name, value string) []int {
	array := toStringArray(value)
	result := make([]int, 0, len(array))
	for _, item := range array {
		result = append(result, toInt(name, item))
	}

	return result
}
