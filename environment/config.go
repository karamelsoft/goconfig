package environment

import (
	"github.com/joho/godotenv"
	"gitlab.com/karamelsoft/goconfig/v2"
	"os"
)

const (
	Mode = "environment"
)

func NewConfiguration() Configuration {
	goconfig.Log("creating environment configuration")

	_ = godotenv.Load()

	return Configuration{}
}

type Configuration struct {
}

func (c Configuration) Set(name, value string) error {
	return os.Setenv(name, value)
}

func (c Configuration) get(name string) stringProperty {
	return stringProperty{
		name:  name,
		value: os.Getenv(name),
	}
}

func (c Configuration) GetString(name string, defaultSupplier func() string) string {
	return c.get(name).toString().or(defaultSupplier)
}

func (c Configuration) GetStringOrCrash(name string) string {
	return c.get(name).toString().or(goconfig.OrMissing[string](name))
}

func (c Configuration) GetStringArray(name string, defaultSupplier func() []string) []string {
	return c.get(name).toStringArray().or(defaultSupplier)
}

func (c Configuration) GetStringArrayOrCrash(name string) []string {
	return c.get(name).toStringArray().or(goconfig.OrMissing[[]string](name))
}

func (c Configuration) GetBool(name string, defaultSupplier func() bool) bool {
	return c.get(name).toBool().or(defaultSupplier)
}

func (c Configuration) GetBoolOrCrash(name string) bool {
	return c.get(name).toBool().or(goconfig.OrMissing[bool](name))
}

func (c Configuration) GetBoolArray(name string, defaultSupplier func() []bool) []bool {
	return c.get(name).toBoolArray().or(defaultSupplier)
}

func (c Configuration) GetBoolArrayOrCrash(name string) []bool {
	return c.get(name).toBoolArray().or(goconfig.OrMissing[[]bool](name))
}

func (c Configuration) GetInt(name string, defaultSupplier func() int) int {
	return c.get(name).toInt().or(defaultSupplier)
}

func (c Configuration) GetIntOrCrash(name string) int {
	return c.get(name).toInt().or(goconfig.OrMissing[int](name))
}

func (c Configuration) GetIntArray(name string, defaultSupplier func() []int) []int {
	return c.get(name).toIntArray().or(defaultSupplier)
}

func (c Configuration) GetIntArrayOrCrash(name string) []int {
	return c.get(name).toIntArray().or(goconfig.OrMissing[[]int](name))
}

func (c Configuration) GetInt32(name string, defaultSupplier func() int32) int32 {
	return c.get(name).toInt32().or(defaultSupplier)
}

func (c Configuration) GetInt32OrCrash(name string) int32 {
	return c.get(name).toInt32().or(goconfig.OrMissing[int32](name))
}

func (c Configuration) GetInt32Array(name string, defaultSupplier func() []int32) []int32 {
	return c.get(name).toInt32Array().or(defaultSupplier)
}

func (c Configuration) GetInt32ArrayOrCrash(name string) []int32 {
	return c.get(name).toInt32Array().or(goconfig.OrMissing[[]int32](name))
}

func (c Configuration) GetInt64(name string, defaultSupplier func() int64) int64 {
	return c.get(name).toInt64().or(defaultSupplier)
}

func (c Configuration) GetInt64OrCrash(name string) int64 {
	return c.get(name).toInt64().or(goconfig.OrMissing[int64](name))
}

func (c Configuration) GetInt64Array(name string, defaultSupplier func() []int64) []int64 {
	return c.get(name).toInt64Array().or(defaultSupplier)
}

func (c Configuration) GetInt64ArrayOrCrash(name string) []int64 {
	return c.get(name).toInt64Array().or(goconfig.OrMissing[[]int64](name))
}

func (c Configuration) GetFloat32(name string, defaultSupplier func() float32) float32 {
	return c.get(name).toFloat32().or(defaultSupplier)
}

func (c Configuration) GetFloat32OrCrash(name string) float32 {
	return c.get(name).toFloat32().or(goconfig.OrMissing[float32](name))
}

func (c Configuration) GetFloat32Array(name string, defaultSupplier func() []float32) []float32 {
	return c.get(name).toFloat32Array().or(defaultSupplier)
}

func (c Configuration) GetFloat32ArrayOrCrash(name string) []float32 {
	return c.get(name).toFloat32Array().or(goconfig.OrMissing[[]float32](name))
}

func (c Configuration) GetFloat64(name string, defaultSupplier func() float64) float64 {
	return c.get(name).toFloat64().or(defaultSupplier)
}

func (c Configuration) GetFloat64OrCrash(name string) float64 {
	return c.get(name).toFloat64().or(goconfig.OrMissing[float64](name))
}

func (c Configuration) GetFloat64Array(name string, defaultSupplier func() []float64) []float64 {
	return c.get(name).toFloat64Array().or(defaultSupplier)
}

func (c Configuration) GetFloat64ArrayOrCrash(name string) []float64 {
	return c.get(name).toFloat64Array().or(goconfig.OrMissing[[]float64](name))
}

func (c Configuration) GetProperties(name string, defaultSupplier func() map[string]string) map[string]string {
	return c.get(name).toProperties().or(defaultSupplier)
}

func (c Configuration) GetPropertiesOrCrash(name string) map[string]string {
	return c.get(name).toProperties().or(goconfig.OrMissing[map[string]string](name))
}
