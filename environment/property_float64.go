package environment

import (
	"log"
	"strconv"
)

type float64Property struct {
	defined bool
	value   float64
}

func (p float64Property) or(supplier func() float64) float64 {
	if p.defined {
		return p.value
	}

	return supplier()
}

type float64ArrayProperty struct {
	value []float64
}

func (p float64ArrayProperty) or(supplier func() []float64) []float64 {
	if p.value == nil {
		return supplier()
	}

	return p.value
}

func toFloat64(name, value string) float64 {
	result, err := strconv.ParseFloat(value, 64)
	if err != nil {
		log.Fatalf("could not convert property (%s = %s) to float64: %v", name, value, err)
	}

	return float64(result)
}

func toFloat64Array(name, value string) []float64 {
	array := toStringArray(value)
	result := make([]float64, 0, len(array))
	for _, item := range array {
		result = append(result, toFloat64(name, item))
	}

	return result
}
