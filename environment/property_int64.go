package environment

import (
	"log"
	"strconv"
)

type int64Property struct {
	defined bool
	value   int64
}

func (p int64Property) or(supplier func() int64) int64 {
	if p.defined {
		return p.value
	}

	return supplier()
}

type int64ArrayProperty struct {
	value []int64
}

func (p int64ArrayProperty) or(supplier func() []int64) []int64 {
	if p.value == nil {
		return supplier()
	}

	return p.value
}

func toInt64(name, value string) int64 {
	result, err := strconv.ParseInt(value, 10, 64)
	if err != nil {
		log.Fatalf("could not convert property (%s = %s) to int64: %v", name, value, err)
	}

	return int64(result)
}

func toInt64Array(name, value string) []int64 {
	array := toStringArray(value)
	result := make([]int64, 0, len(array))
	for _, item := range array {
		result = append(result, toInt64(name, item))
	}

	return result
}
