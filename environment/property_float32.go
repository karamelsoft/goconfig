package environment

import (
	"log"
	"strconv"
)

type float32Property struct {
	defined bool
	value   float32
}

func (p float32Property) or(supplier func() float32) float32 {
	if p.defined {
		return p.value
	}

	return supplier()
}

type float32ArrayProperty struct {
	value []float32
}

func (p float32ArrayProperty) or(supplier func() []float32) []float32 {
	if p.value == nil {
		return supplier()
	}

	return p.value
}

func toFloat32(name, value string) float32 {
	result, err := strconv.ParseFloat(value, 32)
	if err != nil {
		log.Fatalf("could not convert property (%s = %s) to float32: %v", name, value, err)
	}

	return float32(result)
}

func toFloat32Array(name, value string) []float32 {
	array := toStringArray(value)
	result := make([]float32, 0, len(array))
	for _, item := range array {
		result = append(result, toFloat32(name, item))
	}

	return result
}
