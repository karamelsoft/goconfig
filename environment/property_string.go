package environment

import (
	"strings"
)

type stringProperty struct {
	name  string
	value string
}

func (p stringProperty) isUndefined() bool {
	return p.value == ""
}

func (p stringProperty) or(supplier func() string) string {
	if p.isUndefined() {
		return supplier()
	}

	return p.value
}

func (p stringProperty) toString() stringProperty {
	return p
}

func (p stringProperty) toStringArray() stringArrayProperty {
	if p.isUndefined() {
		return stringArrayProperty{
			value: nil,
		}
	}

	return stringArrayProperty{
		value: toStringArray(p.value),
	}
}

func (p stringProperty) toBool() boolProperty {
	if p.isUndefined() {
		return boolProperty{
			defined: false,
		}
	}

	return boolProperty{
		defined: true,
		value:   toBool(p.name, p.value),
	}
}

func (p stringProperty) toBoolArray() boolArrayProperty {
	if p.isUndefined() {
		return boolArrayProperty{
			value: nil,
		}
	}

	return boolArrayProperty{
		value: toBoolArray(p.name, p.value),
	}
}

func (p stringProperty) toInt() intProperty {
	if p.isUndefined() {
		return intProperty{
			defined: false,
		}
	}

	return intProperty{
		defined: true,
		value:   toInt(p.name, p.value),
	}
}

func (p stringProperty) toIntArray() intArrayProperty {
	if p.isUndefined() {
		return intArrayProperty{
			value: nil,
		}
	}

	return intArrayProperty{
		value: toIntArray(p.name, p.value),
	}
}

func (p stringProperty) toInt32() int32Property {
	if p.isUndefined() {
		return int32Property{
			defined: false,
		}
	}

	return int32Property{
		defined: true,
		value:   toInt32(p.name, p.value),
	}
}

func (p stringProperty) toInt32Array() int32ArrayProperty {
	if p.isUndefined() {
		return int32ArrayProperty{
			value: nil,
		}
	}

	return int32ArrayProperty{
		value: toInt32Array(p.name, p.value),
	}
}

func (p stringProperty) toInt64() int64Property {
	if p.isUndefined() {
		return int64Property{
			defined: false,
		}
	}

	return int64Property{
		defined: true,
		value:   toInt64(p.name, p.value),
	}
}

func (p stringProperty) toInt64Array() int64ArrayProperty {
	if p.isUndefined() {
		return int64ArrayProperty{
			value: nil,
		}
	}

	return int64ArrayProperty{
		value: toInt64Array(p.name, p.value),
	}
}

func (p stringProperty) toFloat32() float32Property {
	if p.isUndefined() {
		return float32Property{
			defined: false,
		}
	}

	return float32Property{
		defined: true,
		value:   toFloat32(p.name, p.value),
	}
}

func (p stringProperty) toFloat32Array() float32ArrayProperty {
	if p.isUndefined() {
		return float32ArrayProperty{
			value: nil,
		}
	}

	return float32ArrayProperty{
		value: toFloat32Array(p.name, p.value),
	}
}

func (p stringProperty) toFloat64() float64Property {
	if p.isUndefined() {
		return float64Property{
			defined: false,
		}
	}

	return float64Property{
		defined: true,
		value:   toFloat64(p.name, p.value),
	}
}

func (p stringProperty) toFloat64Array() float64ArrayProperty {
	if p.isUndefined() {
		return float64ArrayProperty{
			value: nil,
		}
	}

	return float64ArrayProperty{
		value: toFloat64Array(p.name, p.value),
	}
}

func (p stringProperty) toProperties() properties {
	if p.isUndefined() {
		return properties{
			value: nil,
		}
	}

	return properties{
		value: ToProperties(p.name, p.value),
	}
}

type stringArrayProperty struct {
	value []string
}

func (p stringArrayProperty) or(supplier func() []string) []string {
	if p.value == nil {
		return supplier()
	}

	return p.value
}

func toStringArray(value string) []string {
	return strings.Split(value, ",")
}
