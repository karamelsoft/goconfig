package environment

import (
	"log"
	"strconv"
)

type int32Property struct {
	defined bool
	value   int32
}

func (p int32Property) or(supplier func() int32) int32 {
	if p.defined {
		return p.value
	}

	return supplier()
}

type int32ArrayProperty struct {
	value []int32
}

func (p int32ArrayProperty) or(supplier func() []int32) []int32 {
	if p.value == nil {
		return supplier()
	}

	return p.value
}

func toInt32(name, value string) int32 {
	result, err := strconv.ParseInt(value, 10, 32)
	if err != nil {
		log.Fatalf("could not convert property (%s = %s) to int32: %v", name, value, err)
	}

	return int32(result)
}

func toInt32Array(name, value string) []int32 {
	array := toStringArray(value)
	result := make([]int32, 0, len(array))
	for _, item := range array {
		result = append(result, toInt32(name, item))
	}

	return result
}
