package environment

import (
	"encoding/json"
	"log"
)

type properties struct {
	value map[string]string
}

func (p properties) or(supplier func() map[string]string) map[string]string {
	if p.value != nil {
		return p.value
	}

	return supplier()
}

func ToProperties(name, value string) map[string]string {
	props := make(map[string]string)
	if err := json.Unmarshal([]byte(value), &props); err != nil {
		log.Fatalf("could not read properties %s: %v", name, err)
	}

	return props
}
