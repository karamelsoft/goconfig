package docker

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func Test_pathOf(t *testing.T) {
	require.Equal(t, "/run/secrets/file.txt", pathOf("file.txt"))
}
