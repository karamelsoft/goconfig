package docker

import (
	"gitlab.com/karamelsoft/goconfig/v2"
	"gitlab.com/karamelsoft/goconfig/v2/file"
	"gitlab.com/karamelsoft/goconfig/v2/inmemory"
	"path"
)

const (
	Mode                   = "docker"
	secretsRootPath        = "/run/secrets"
	defaultSecretsFileName = file.DefaultConfigurationFile
)

func pathOf(fileName string) string {
	return path.Join(secretsRootPath, fileName)
}

func NewConfiguration() *inmemory.Configuration {
	goconfig.Log("creating docker configuration")

	return file.NewConfigurationIfExists(pathOf(defaultSecretsFileName))
}

func NewConfigurationUsing(fileName string) func() *inmemory.Configuration {
	goconfig.Log("creating docker configuration using file:", fileName)
	return func() *inmemory.Configuration {
		return file.NewConfigurationIfExists(pathOf(fileName))
	}
}
