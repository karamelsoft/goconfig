package factory

import (
	"gitlab.com/karamelsoft/goconfig/v2"
	"gitlab.com/karamelsoft/goconfig/v2/cli"
	"gitlab.com/karamelsoft/goconfig/v2/docker"
	"gitlab.com/karamelsoft/goconfig/v2/environment"
	"gitlab.com/karamelsoft/goconfig/v2/fallback"
	"gitlab.com/karamelsoft/goconfig/v2/file"
	"log"
)

const (
	configurationMode        = "CONFIGURATION_MODE"
	defaultConfigurationMode = environment.Mode
)

func NewConfiguration(applicationName string) goconfig.Configuration {
	env := environment.NewConfiguration()
	mode := env.GetString(configurationMode, goconfig.Or(defaultConfigurationMode))
	switch mode {
	case cli.Mode:
		return fallback.NewConfiguration(
			cli.NewConfiguration(applicationName),
			env,
		)
	case docker.Mode:
		return fallback.NewConfiguration(
			docker.NewConfiguration(),
			env,
		)
	case environment.Mode:
		return env
	case file.Mode:
		return fallback.NewConfiguration(
			file.NewConfigurationFrom(env),
			env,
		)
	default:
		log.Fatalf("unknown %s: %s", configurationMode, mode)
		return nil
	}
}
